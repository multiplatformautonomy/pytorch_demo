import copy
import gym
import scrimmage.utils
import random


def get_action(obs):
    #of actions = 5 (0,1,2,3,4)
    return random.randint(0, 4)

# Used for non-learning mode
def return_action_func(action_space, obs_space, params):
    return get_action(obs_space)

def test_openai():
    try:
        env = gym.make('scrimmage-ctf-v0')
    except gym.error.Error:
        mission_file = scrimmage.utils.find_mission('ctf-pytorch.xml')

        gym.envs.register(
            id='scrimmage-ctf-v0',
            entry_point='scrimmage.bindings:ScrimmageOpenAIEnv',
            max_episode_steps=1e9,
            reward_threshold=1e9,
            kwargs={"enable_gui": False,
                    "mission_file": mission_file}
        )
        env = gym.make('scrimmage-ctf-v0')

    # the observation is the x position of the vehicle
    # note that a deepcopy is used when a history
    # of observations is desired. This is because
    # the sensor plugin edits the data in-place
    obs = []
    temp_obs = copy.deepcopy(env.reset())
    print("reset - temp_obs: ", temp_obs)
    obs.append(temp_obs)
    total_reward = 0
    for i in range(5):
        action = get_action(temp_obs)
        print("count =", i, ", action=", action, "obs =", temp_obs)
        temp_obs, reward, done = env.step(action)[:3]
        print("reward =", reward, ", action=", action, ", obs =", temp_obs)
        obs.append(copy.deepcopy(temp_obs))
        total_reward += reward

        if done:
            break

    env.close()
    print("Total Reward: %2.2f" % total_reward)

if __name__ == '__main__':
    test_openai()