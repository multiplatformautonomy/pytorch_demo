#include <pytorch-demo/plugins/sensor/CTFSensor/CTFSensor.h>

#include <scrimmage/entity/Contact.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/math/State.h>
#include <scrimmage/common/Time.h>
#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/plugins/sensor/ScrimmageOpenAISensor/ScrimmageOpenAISensor.h>
#include <scrimmage/sensor/Sensor.h>

#include <iostream>
#include <map>
#include <string>
#include <sstream>

using std::cout;
using std::endl;

namespace sc = scrimmage;

REGISTER_PLUGIN(scrimmage::Sensor,
                scrimmage::sensor::CTFSensor,
                CTFSensor_plugin)

namespace scrimmage {
namespace sensor {
    void CTFSensor::init(std::map<std::string, std::string> &params) {
        // std::string visentlist = params["visible_entities"];
        // std::stringstream stream(visentlist);
        // std::string nums = "0123456789";
        // for (uint32_t i; stream >> i;) {
        //     visible_entities_.push_back(i);
        //     if(std::string::npos != nums.find(stream.peek(),0)) {
        //         stream.ignore();
        //     }
        // }
    }

    void CTFSensor::get_observation(double* data, uint32_t beg_idx, uint32_t end_idx) {
        uint32_t beginning = beg_idx;
        // Set observation state based on current state and other entities' states.
        data[beginning] = parent_->state()->pos()(0);
        data[beginning+1] = parent_->state()->pos()(1);
        // std::cout << "! --- CTFSensor get_observation - data[beginning+1]=" << data[beginning+1] << std::endl;
        // data[beginning+2] = parent_->state()->pos()(2);
        // data[beginning+3] = time_->t();
        // uint32_t new_beginning = beginning + 4;
        // auto cmap = parent_->contacts();
        // for(auto key : visible_entities_) {
        //     auto valiter = cmap->find(key);
        //     // If the key was found:
        //     if (valiter != cmap->end()) {
        //         Eigen::Vector3d entpos = (*cmap)[key].state()->pos();
        //         data[new_beginning] = entpos(0);
        //         data[new_beginning+1] = entpos(1);
        //         data[new_beginning+2] = entpos(2);
        //     }
        //     else {
        //         data[new_beginning] = 0;
        //         data[new_beginning+1] = 0;
        //         data[new_beginning+2] = 0;
        //     }
        //     new_beginning += 3;
        // }
    }

	void CTFSensor::set_observation_space() {
	    const double inf = std::numeric_limits<double>::infinity();
	    observation_space.continuous_extrema.push_back(std::make_pair(-inf, inf));
	    observation_space.continuous_extrema.push_back(std::make_pair(-inf, inf));
        // std::cout << "! --- CTFSensor set_observation_space" <<std::endl;
	    // observation_space.continuous_extrema.push_back(std::make_pair(-inf, inf));
	    // observation_space.continuous_extrema.push_back(std::make_pair(0, inf));
        // for(uint32_t i = 0; i < visible_entities_.size(); i++) {
        //     observation_space.continuous_extrema.push_back(std::make_pair(-inf, inf));
        //     observation_space.continuous_extrema.push_back(std::make_pair(-inf, inf));
        //     observation_space.continuous_extrema.push_back(std::make_pair(-inf, inf));
        // } 
	}
} // namespace sensor
} // namespace scrimmage
