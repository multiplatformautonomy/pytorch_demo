/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#include <pytorch-demo/plugins/autonomy/CTFAutonomy/CTFAutonomy.h>

#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/math/State.h>
#include <scrimmage/parse/ParseUtils.h>
#include <scrimmage/pubsub/Publisher.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <scrimmage/plugins/interaction/Boundary/BoundaryBase.h>
#include <scrimmage/plugins/interaction/Boundary/Boundary.h>
#include <scrimmage/common/Waypoint.h>
#include <scrimmage/plugins/autonomy/WaypointGenerator/WaypointList.h>
#include <scrimmage/msgs/Capture.pb.h>

#include <iostream>
#include <limits>
#include <GeographicLib/LocalCartesian.hpp>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;
namespace sci = scrimmage::interaction;

REGISTER_PLUGIN(scrimmage::Autonomy,
                scrimmage::autonomy::CTFAutonomy,
                CTFAutonomy_plugin)

namespace scrimmage {
namespace autonomy {

  void CTFAutonomy::init_helper(std::map<std::string, std::string> &params) {
    // using Type = scrimmage::VariableIO::Type;
    // using Dir = scrimmage::VariableIO::Direction;

    output_vel_x_idx_ = vars_.declare(VariableIO::Type::velocity_x, VariableIO::Direction::Out);
    output_vel_y_idx_ = vars_.declare(VariableIO::Type::velocity_y, VariableIO::Direction::Out);
    output_vel_z_idx_ = vars_.declare(VariableIO::Type::velocity_z, VariableIO::Direction::Out);

    // radius_ = std::stod(params.at("radius")); handle radius - close enough for take flag .. etc in interaction

    action_options_[0] = std::make_pair(0,1); //forward
    action_options_[1] = std::make_pair(0,-1); //back
    action_options_[2] = std::make_pair(-1,0); //left
    action_options_[3] = std::make_pair(1,0); //right
    action_options_[4] = std::make_pair(0,0); //stationary

    flag_boundary_id_ = get<int>("flag_boundary_id", params, flag_boundary_id_);
    capture_boundary_id_ = get<int>("capture_boundary_id", params, capture_boundary_id_);

    auto callback = [&] (scrimmage::MessagePtr<sp::Shape> msg) {
        std::cout << "! --- CTFAutonomy callback for Boundary" << std::endl;
        std::shared_ptr<sci::BoundaryBase> boundary = sci::Boundary::make_boundary(msg->data);
        boundaries_[msg->data.id().id()] = std::make_pair(msg->data, boundary);
    };
    // subscribe<sp::Shape>("GlobalNetwork", "Boundary", callback);

    //has enemy flag
    auto flag_taken_cb = [&] (scrimmage::MessagePtr<sm::FlagTaken> msg) {
        if (msg->data.entity_id() == parent_->id().id() &&
            msg->data.flag_boundary_id() == flag_boundary_id_) {
            has_flag_ = true;
        }
    };
    subscribe<sm::FlagTaken>("GlobalNetwork", "FlagTaken", flag_taken_cb);

    // std::cout << "! --- CTFAutonomy init_helper (2) - flag_boundary_id_=" << flag_boundary_id_ << ", action_options_.size()="<<action_options_.size()<<std::endl;
  }

  void CTFAutonomy::set_environment() {
      reward_range = std::make_pair(0, 2);
      // action space (2D) 0: forward, 1: back, 2: left, 3: right, 4: stationary
      action_space.discrete_count.push_back(5);
      // std::cout << "! --- CTFAutonomy set_environment - reward_range=" << reward_range.first << ", action_space.discrete_count[0]="<<action_space.discrete_count[0]<<std::endl;
      //discrete - only positive values. if use continuous... -1 min to 1 max

      // obs --------
      // const double inf = std::numeric_limits<double>::infinity();
      // observation_space.continuous_extrema.push_back(std::make_pair(-inf, inf));
  }

  double CTFAutonomy::find_enemy_dist(){
    // Find the closest enemy entity (BoundaryDefense)
    double min_dist = std::numeric_limits<double>::infinity();
    auto it = boundaries_.find(capture_boundary_id_); // capture_boundary_id_ = 1 (blue's boundary)
    if (it == boundaries_.end()) {
        return min_dist;
    }

    sc::StatePtr closest = nullptr;
    for (auto &kv : *contacts_) { // look for entities is blue's boundary
        sc::Contact &cnt = kv.second;
        // Ignore same team
        if (cnt.id().team_id() == std::get<0>(it->second).id().team_id()) {
            continue;
        }
        if (closest == nullptr ||
            (state_->pos() - closest->pos()).norm() < min_dist) {
            closest = cnt.state();
            min_dist = (state_->pos() - closest->pos()).norm();
        }
    }
    return min_dist;
  }
/*
At every time step, there are the observable states (plus calculated)
Fixed:
  Agent's flag (flag_x, flag_y), enemy flag (e_flag_x, e_flag_y)
  Velocity (x_dot) - constant at 1 vx or 1 vy
  Game Boundary: 0 to 100

Starting State (reset):
  Flag pos at fixed. Agent and Enemy agent at fixed pos.
Episode length > than Z
Termination(Done):
  Agent captures flag (within range)
  Exceed mission play time
  Moves out of bounds (simple - prevent oobounds; complex - factor in reward)
    Python: get_action(obs) - keep action w/in bounds
  *Captured (complex: Future scenario)
Optimal Policy - at each state, find best action to take to maximize reward
Option:
  Ignore OpenAISensor. Implement observation w/in CTFAutonomy
*/
  std::tuple<bool, double, pybind11::dict> CTFAutonomy::calc_reward() {
      // std::cout << "! --- CTFAutonomy calc_reward - flag_boundary_id_=" << flag_boundary_id_<< std::endl;
      double flag_dist = std::numeric_limits<double>::infinity();
      auto it = boundaries_.find(flag_boundary_id_);
      // should only be one enemy flag.
      if (it != boundaries_.end()) {
          Eigen::Vector2d flag_pos = std::get<1>(it->second)->center().head<2>();
          // get the distance between the agent and the enemy flag
          flag_dist = (flag_pos - state_->pos().head<2>()).norm();
      }
      else{
        // std::cout << "! --- CTFAutonomy calc_reward - flag_boundary_id_ NOT found" << std::endl;
      }
  
      // double enemy_dist = find_enemy_dist();
      // std::cout << "!! --- CTFAutonomy calc_reward - flag_dist=" << flag_dist << ", enemy_dist=" << enemy_dist << std::endl;
      // double reward = (1 - flag_dist) + (enemy_dist); // reward range: [0-2]
      double reward = 1;
      // std::cout << "! --- CTFAutonomy calc_reward - reward=" << reward << std::endl;

      // data[0] = parent_->state()->pos()(0);
      // data[1] = parent_->state()->pos()(1);

      // Python: Store experience replay, Q-value, DQN implementation, etc
      // experience replay memory for training our DQN

      // obs is set as a list [] first idx is x pos and second idx is time
      pybind11::dict info;
      info["flag_distance"] = flag_dist; // debugging information
      return std::make_tuple(has_flag_, reward, info);
  }

  bool CTFAutonomy::step_helper() {
      // action.discrete[0] values can be 0 to 4. first=x, second=y
      // python script will call action and pass value - set action here. so retrieve and set for motion.
      // std::cout << "! --- CTFAutonomy step_helper - action.discrete[0]=" << action.discrete[0] <<std::endl;
      const double x_vel = action_options_[action.discrete[0]].first;
      const double y_vel = action_options_[action.discrete[0]].second;
      std::cout << "CTFAutonomy action=" << action.discrete[0] << ", (x, y) = ("<<x_vel<<","<<y_vel<<")"<<std::endl;

      vars_.output(output_vel_x_idx_, x_vel);
      vars_.output(output_vel_y_idx_, y_vel);
      return true;
  }

} // namespace autonomy
} // namespace scrimmage
